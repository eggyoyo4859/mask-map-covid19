import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import * as firebase from 'firebase';
import 'firebase/firestore';
import mapboxgl from 'mapbox-gl'
import {MenuOutlined,PushpinOutlined,ShopOutlined} from '@ant-design/icons';
import {
    Drawer,
    Button,
    Select,
    Modal,
    List,
    Form,
    Input,
    Tabs,
    Spin,
    message
} from 'antd';
import {EditShopModalForm} from "./editShop";
import {CreateShopModalForm} from "./createShop";
import {InfoPopup} from "./infoPopup";
import L,{DivIcon} from 'leaflet'
import shortid from 'shortid'
import './App.css';
import {
    editDataByID,
    isAuthenticated,
    loginWithFacebook,
    loginWithGoogle,
    logout,
    getAllShop,
    createNewShopData,
    parseGeoPoint,
    getDataByID,
    findShopNearMe,
    getTop3LowPriceShop,
    getTop3MoreStockShop,
    loginWithFacebookRedirect
} from './firebase'
import geohash from 'ngeohash'
//import {ReactComponent as LegendPic} from './mask-legend.png'

const {Option} = Select;
const {TabPane} = Tabs;
var db = firebase.firestore()

mapboxgl.accessToken = 'pk.eyJ1IjoiZWdneW80ODU5IiwiYSI6ImNpb2wzaHFkYTAwM291MG0wNWZ4cW5sMGcifQ.yVv_6LIdOz' +
        '8bp5ky9YDAtw'

const colorByLevel = (quantity) => {
    var color = '#262626'
    if (quantity > 0 && quantity <= 500) {
        color = '#851d41'
    } else if (quantity > 500 && quantity <= 2500) {
        color = '#df8543'
    } else if (quantity > 2500 && quantity <= 5000) {
        color = '#ecce6d'
    } else if (quantity > 5000 && quantity <= 10000) {
        color = '#4dd599'
    } else if (quantity > 10000) {
        color = '#3fc5f0'
    } else {
        color = '#262626'
    }
    return color
}
export default class App extends Component {
    map;
    centerMarker;
    unsubscribe;


    constructor(props, context) {
        super(props, context)
        this.state = {
            isFindBtnLoading:false,
            isMainLoad: true,
            isDrawerVisible: false,
            isCreateModalVisible: false,
            isEditModalVisible: false,
            isInfoModalVisible: false,
            isEditLoading: false,
            isAuth: true,
            currentUser: null,
            isAnonymousMode: false,
            markerGeoPoint: null,
            myShopData: [],
            selectedMarker:null,
            data: null,
            top3StockData:[],
            top3PriceData:[],
        }
    }
    componentDidMount = async() => {
        this.checkAuthenticate()
        //this.renderMap()
    }
    checkAuthenticate = async() => {
        const res = await isAuthenticated()
        this.renderMap()
        this.makeCenterMarker()

        if (res.user) {
            this.setState({
                isAuth: true,
                currentUser: res.user,
                isMainLoad: false
            }, () => {
                this.listenShopData(res.user.uid)
                this.addMarkerCenter()
                this.renderLeafletCreateShopBtn()
            })
        } else {
            this.renderLeafletFindBtn()
            this.listenShopData()
            this.setState({isAuth: false, isMainLoad: false})
        }

    }
    listenShopData = async(uid) => {
        const that = this
        var query = db.collection("shop")
        if (uid) {
            query = query.where('owner', '==', uid)
        }
        this.unsubscribe = query.onSnapshot(async function (snapshot) {
            that.getMyShop(uid)
            that.getTop3Price()
            that.getTop3Stock()
            snapshot
                .docChanges()
                .forEach(function (change) {
                    const data = change
                        .doc
                        .data();
                    if (change.type === "added") {
                        console.log("New shop: ", data);
                        var myIcon = L.divIcon({id: data.id,html:that.renderMarkerDOM(data)});
                        L.marker([data.geo.F, data.geo.V], {icon: myIcon}).addTo(that.map);
                    }
                    if (change.type === "modified") {
                        console.log("Modified shop: ", data);
                        const element = document.getElementById(data.id)
                        element
                            .parentNode.parentNode
                            .removeChild(element.parentNode);
                            var myIcon = L.divIcon({id: data.id,html:that.renderMarkerDOM(data)});
                            L.marker([data.geo.F, data.geo.V], {icon: myIcon}).addTo(that.map);

                    }
                    if (change.type === "removed") {
                        console.log("Removed shop: ", data);
                        const element = document.getElementById(data.id)
                        element
                            .parentNode.parentNode
                            .removeChild(element.parentNode);
                    }
                });
        });
    }

    getMyShop = async(uid) => {
        const res = await getAllShop(uid)
        if (res.type === 'success') {
            this.setState({myShopData: res.results})
        } else {
            this.showMessage(res.type, res.msg)
        }
    }
    clearAllMarker = () => {
        var paras = document.getElementsByClassName('shop-marker');
        while (paras[0]) {
            paras[0]
                .parentNode
                .removeChild(paras[0]);
        }
    }
    getTop3Price = async() => {
      const res = await getTop3LowPriceShop()
      if(res.results){
        this.setState({top3PriceData:res.results})
      }
    }
    getTop3Stock = async() => {
      const res = await getTop3MoreStockShop()
      if(res.results){
        this.setState({top3StockData:res.results})
      }
    }

    renderMarkerDOM = (data) => {
      const that = this
        var el = document.createElement('div');
        el.id = data.id
        el.className = 'shop-marker';
        el.style.backgroundColor = colorByLevel(data.quantity)
        el.style.marginLeft = '-9px'
        el.style.marginTop = '-9px'
        var fontSize = 15
        var marginTop = 3
        if (`${data.price}`.length >= 3) {
            fontSize = 12
            marginTop = 4
        }
        el.innerHTML = `<p style='width:100%;color:white;font-weight:bold;font-size:${fontSize}px;margin-top:${marginTop}px'>${data.price}</p>`

        el.addEventListener('click', function () {
          that.setState({selectedMarker:data},()=>{
            that.showModal('info')
          })
            
        });
        return el
    }

    renderMap(){
        const _this = this;

        const position = [13.736717,100.523186]
        this.map = L.map('map', {
            center: position,
            zoomControl: false,
            zoom: 6,
            layers: [
              L.tileLayer('https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png', {
                attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>',
                maxZoom: 19
            }),
            ]
          });
          const onMoveEnd = () => {
            const center = this.map.getCenter()
            this.centerMarker.setLatLng(center)
            this.setState({markerGeoPoint: center})
            }
          this.map.on('move',onMoveEnd)
          L.Control.MyControl = L.Control.extend({
            onAdd: function(map) {
              var el = L.DomUtil.create('div', 'my-control');
              ReactDOM.render( _this.renderMenuBtn(), el );
              return el;
            },
          });
          
          L.control.myControl = function(opts) {
            return new L.Control.MyControl(opts);
          }
          
          L.control.myControl({
            position: 'topleft'
          }).addTo(this.map);

          

    }

    renderLeafletFindBtn(){
        this.refreshBtn()
        const _this = this;

        L.Control.MyControl = L.Control.extend({
            onAdd: function(map) {
              var el = L.DomUtil.create('div', 'leaflet-ui-btn');
              ReactDOM.render( _this.renderFindNearBtn(), el );
              return el;
            },
          });
          
          L.control.myControl = function(opts) {
            return new L.Control.MyControl(opts);
          }
          
          L.control.myControl({
            position: 'bottomright'
          }).addTo(this.map);
    }

    renderLeafletCreateShopBtn(){
        this.refreshBtn()
        const _this = this;

        L.Control.MyControl = L.Control.extend({
            onAdd: function(map) {
              var el = L.DomUtil.create('div', 'leaflet-ui-btn');
              ReactDOM.render( _this.renderCreateShopBtn(), el );
              return el;
            },
          });
          
          L.control.myControl = function(opts) {
            return new L.Control.MyControl(opts);
          }
          
          L.control.myControl({
            position: 'bottomright'
          }).addTo(this.map);
    }
    refreshBtn(){
        var x = document.getElementsByClassName("leaflet-ui-btn");
        while(x.length > 0){
            x[0].parentNode.removeChild(x[0]);
        }
    }




    makeCenterMarker = ()=> {
      console.log(this.map.getCenter())
      const center = this.map.getCenter()
      this.centerMarker = L.marker([center.lat, center.lng]).addTo(this.map);
    }
    addMarkerCenter(){
      const center = this.map.getCenter()
      console.log(center)
      this.centerMarker.setLatLng(center).addTo(this.map);
    }
    removeMarkerCenter(){
      this.centerMarker.remove()
    }

    showMessage = (type, msg) => {
        if (type === 'success') {
            message.success(msg)
        } else if (type === 'error') {
            message.error(msg)
        } else {
            message.warning(msg)
        }
    }
    flyToId= async(id)=> {
      const res = await getDataByID(id)
      if(res.data){
          console.log(`res.data.geo:${JSON.stringify(res.data.geo)}`)
        this.map.flyTo([res.data.geo.F,res.data.geo.V],12);
      }
      
    }

    onShowDrawer = () => {
        this.setState({isDrawerVisible: true});
    };
    onDrawerClose = () => {
        this.setState({isDrawerVisible: false});
    };
    onSearchChange = (value) => {
        this.onDrawerClose()
        this.flyToId(value)
        console.log(`selected ${value}`);
    }

    onSearchBlur = () => {
        console.log('blur');
    }

    onSearchFocus = () => {
        console.log('focus');
    }

    onSearch = (val) => {
        console.log('search:', val);
    }

    showModal = (type) => {
        if (type === 'create') {
            this.setState({isCreateModalVisible: true});
        } else if(type === 'edit'){
            this.setState({isEditModalVisible: true});
        } else {
            this.setState({isInfoModalVisible: true})
        }
    };

    handleEditModalOk = async(e) => {
        console.log(`e:${JSON.stringify(e)}`);
        e.updatedAt = new Date().getTime()
        e.quantity = parseInt(e.quantity)
        e.price = parseFloat(e.price)
        if(e.lat){
          e.geoHash = geohash.encode(e.lat, e.lng)
        }
        const res = await editDataByID(e)
        this.showMessage(res.type, res.msg)
        this.setState({isEditModalVisible: false});
    };

    handleCreateModalOk = async(e) => {
        console.log(`e:${JSON.stringify(e)}`);
        e.createdAt = new Date().getTime()
        e.updatedAt = new Date().getTime()
        e.owner = this.state.currentUser.uid
        e.id = shortid.generate()
        e.geo = parseGeoPoint(this.state.markerGeoPoint)
        e.geoHash = geohash.encode(this.state.markerGeoPoint.lat, this.state.markerGeoPoint.lng)
        e.quantity = parseInt(e.quantity)
        e.price = parseFloat(e.price)
        const res = await createNewShopData(e)
        this.showMessage(res.type, res.msg)
        this.setState({isCreateModalVisible: false});
    };

    handleModalCancel = (type) => {
        if (type === 'create') {
            this.setState({isCreateModalVisible: false});
        } else if(type === 'edit'){
            this.setState({isEditModalVisible: false});
        }else{
          this.setState({isInfoModalVisible: false})
        }
    };

    googleAuth = async() => {
        const res = await loginWithGoogle()
        if (res.user) {
            this.setState({
                currentUser: res.user,
                isAuth: true
            }, () => {
                this.editorMode()
            })
            this.editorMode()
        }
        this.showMessage(res.type, res.msg)
    }

    facebookAuth = async() => {
        const res = await loginWithFacebookRedirect()
        // if (res.user) {
        //     this.setState({
        //         currentUser: res.user,
        //         isAuth: true
        //     }, () => {
        //         this.editorMode()
        //     })
        // }
        // this.showMessage(res.type, res.msg)
    }
    handleLogout = async() => {
        const res = await logout()
        if (res.type === 'success') {
            this.setState({
                currentUser: null,
                isAuth: false
            }, () => {
                this.anonymousMode()
            })
        }
        this.showMessage(res.type, res.msg)
    }

    editorMode = async() => {
        this.setState({isAnonymousMode: false,myShopData:[]})
        this.clearAllMarker()
        this.addMarkerCenter()
        this.renderLeafletCreateShopBtn()
        this.unsubscribe()
        this.listenShopData(this.state.currentUser.uid)
    }
    anonymousMode = async() => {
        this.setState({isAnonymousMode: true,myShopData:[]})
        this.clearAllMarker()
        this.removeMarkerCenter()
        this.renderLeafletFindBtn()
        this.unsubscribe()
        this.listenShopData()
    }

    handleGoogleMap =()=> {
      const {selectedMarker} = this.state
      window.open(`https://maps.google.com/?q=${selectedMarker
        ? selectedMarker.geo.F + ',' + selectedMarker.geo.V
        : ''}`,'_blank')
    }

    handleCall=() => {
      const {selectedMarker} = this.state
      window.location.href = `tel:${selectedMarker
        ? selectedMarker.phone
        : ''}`;
    }

    renderEditShopModal() {
        return (<EditShopModalForm
            visible={this.state.isEditModalVisible}
            data={this.state.data}
            onSubmit={this.handleEditModalOk}
            onCancel={this.handleModalCancel}/>)
    }

    renderInfoPopupModal() {
      return (<InfoPopup
          visible={this.state.isInfoModalVisible}
          data={this.state.selectedMarker}
          onGoogleMap={this.handleGoogleMap}
          onCall={this.handleCall}
          onCancel={this.handleModalCancel}/>)
  }

    renderCreateShopModal() {
        return (<CreateShopModalForm
            visible={this.state.isCreateModalVisible}
            onSubmit={this.handleCreateModalOk}
            onCancel={this.handleModalCancel}/>)
    }

    renderClientDrawer() {
        return (
            <Drawer
                title="ค้นหาร้าน"
                placement='left'
                width={280}
                closable={false}
                onClose={this.onDrawerClose}
                visible={this.state.isDrawerVisible}
                footer={(
                <div style={{
                    textAlign: 'center'
                }}>
                    {this.state.isAuth
                        ? (
                            <Button
                                block
                                type="primary"
                                onClick={() => {
                                this.editorMode()
                            }}>โหมดแก้ไขข้อมูล</Button>
                        )
                        : (
                            <div>
                                <p
                                    style={{
                                    fontSize: 13
                                }}>*สำหรับร้านที่ต้องการแชร์ข้อมูลกรุณา Log in</p>
                                <Button block type="primary" onClick={this.facebookAuth}>Log in with Facebook</Button>
                                <Button
                                    block
                                    type="danger"
                                    style={{
                                    marginTop: 12
                                }}
                                    onClick={this.googleAuth}>Log in with Gmail</Button>
                            </div>
                        )}
                </div>
            )}>
                <Select
                    showSearch
                    style={{
                    width: '100%',
                    marginBottom:30
                }}
                    placeholder="ค้นหาชื่อร้าน"
                    optionFilterProp="children"
                    onChange={this.onSearchChange}
                    onFocus={this.onSearchFocus}
                    onBlur={this.onSearchBlur}
                    onSearch={this.onSearch}
                    filterOption={(input, option) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
                    {this.state.myShopData.map((data)=>{
                      return(<Option value={data.id}>{data.name}</Option>)
                    })}
                </Select>
                <List
                    size="small"
                    header={'ราคาถูกสุด'}
                    dataSource={this.state.top3PriceData}
                    renderItem={item => <List.Item>
                    <a
                        href="#"
                        onClick={() => {
                        this.setState({
                            selectedMarker: item
                        }, () => {
                          this.showModal('info')
                          this.onDrawerClose();
                          this.flyToId(item.id)
                        })
                    }}>{`${item.name} - ${item.price} บ.`}</a>
                </List.Item>}/>
                <List
                    size="small"
                    header={'หน้ากากเยอะสุด'}
                    dataSource={this.state.top3StockData}
                    renderItem={item => <List.Item>
                    <a
                        href="#"
                        onClick={() => {
                        this.setState({
                            selectedMarker: item
                        }, () => {
                          this.showModal('info')
                          this.onDrawerClose();
                          this.flyToId(item.id)
                        })
                    }}>{`${item.name} - ${item.quantity} ชิ้น`}</a>
                </List.Item>}/>
            </Drawer>
        )
    }
   
    renderAdminDrawer() {
        return (
            <Drawer
                title="ร้านของฉัน"
                placement='left'
                width={280}
                closable={false}
                onClose={this.onDrawerClose}
                visible={this.state.isDrawerVisible}
                footer={(
                <div style={{
                    textAlign: 'center'
                }}>
                    <Button
                        block
                        type="primary"
                        onClick={() => {
                        this.anonymousMode()
                    }}>โหมดผู้ใช้งานทั่วไป</Button>
                    <Button
                        block
                        onClick={this.handleLogout}
                        style={{
                        marginTop: 12
                    }}>Log out</Button>
                </div>
            )}>
                <List
                    size="small"
                    dataSource={this.state.myShopData}
                    renderItem={item => <List.Item>
                    <a
                        href="#"
                        onClick={() => {
                          
                          item.lat = item.geo.F
                          item.lng = item.geo.V
                        this.setState({
                            data: item
                        }, () => {
                          console.log(`this.state.data:${JSON.stringify(this.state.data)}`);
                          this.showModal('edit')
                          this.onDrawerClose();
                        })
                    }}>{item.name}</a>
                </List.Item>}/>
            </Drawer>
        )
    }

    getUserLocation = async()=>{
      if (navigator.geolocation) { 
        this.setState({isFindBtnLoading:true})
        message.loading({ content: 'ค้นหาร้านใกล้ฉัน...',key:'find-shop',duration: 10});
        navigator.geolocation.getCurrentPosition(async (position,error)=> { 
          console.log(position.coords.latitude, position.coords.longitude); 
          if(position.coords.latitude){
            const res = await findShopNearMe(position.coords.latitude,position.coords.longitude)
            if(res.type === 'success'){
                message.success({ content: 'เจอร้านใกล้คุณแล้ว!',key:'find-shop'});
              this.setState({isFindBtnLoading:false})
              const shopId = res.results.id
              this.flyToId(shopId)
            }else{
                message.error({ content: 'ไม่เจอร้านใกล้คุณเลย!',key:'find-shop'});
              this.setState({isFindBtnLoading:false})
              message.error(res.msg)
            }
            console.log(`Near me : ${JSON.stringify(res)}`); 
          }else{
            message.error({ content: 'กรุณากดอนุญาตให้ใช้ตำแหน่ง!',key:'find-shop'});
            this.setState({isFindBtnLoading:false})
            message.error('กรุณากดปุ่มหาตำแหน่งของคุณ ด้าน ขวาบน')
          }
          
        }); 
      }else{
        message.error('Web Browser นี้ไม่รองรับการค้นหาตำแหน่ง')
      }
    }
    renderFindNearBtn() {
        return (
            <Button
                className="shadow-darken25"
                style={{
                    width: 64,
                    height: 64,
                textAlign: 'center',
                stroke:'solid',
                borderWidth:2,
                borderColor:'white',
                backgroundColor:'#08979c'
            }}
                icon={<ShopOutlined style={{fontSize:24,color:'white'}}/>}
                loading={this.state.isFindBtnLoading}
                shape="circle"
                size="large"
                onClick={() => {
                console.log('test');
                this.getUserLocation()
            }}></Button>
        )
    }
    renderCreateShopBtn() {
        return (
            <Button
                className="shadow-darken25"
                style={{
                    width: 64,
                    height: 64,
                textAlign: 'center',
                stroke:'solid',
                borderWidth:2,
                borderColor:'white',
                backgroundColor:'#08979c'
            }}
                icon={<PushpinOutlined style={{fontSize:24,color:'white'}}/>}
                shape="circle"
                size="large"
                onClick={() => {
                console.log('test');
                this.showModal('create')
            }}></Button>
        )
    }
    renderMenuBtn(){
        return(
            <Button style={{
            width: 40,
            height: 30,
            textAlign: 'center'
        }}
            icon={<MenuOutlined style={{color:'#1890ff'}}/>}
            className="shadow-darken10"
            onClick={() => {
            console.log('test');
            this.onShowDrawer()
        }}></Button>
            
        )
    }

    render() {
        return (<div id="map">
            {this.state.isAuth && !this.state.isAnonymousMode
                                ? this.renderAdminDrawer()
                                : this.renderClientDrawer()}
                                {this.renderCreateShopModal()}
                            {this.renderEditShopModal()}
                            {this.renderInfoPopupModal()}
        </div>)
    }
}
