import React, {Component} from 'react';
import {
    Modal,
    Button,
    Row,
    Col,
    Card,
    Statistic,
    Tabs,
    Rate,
    message,
    Input
} from "antd";
import {verifyShop,reportShop,createReportData,createVerifyData,sendAnalyze} from './firebase'
import moment from 'moment'
import {localization} from './Localization'
import {SafetyCertificateOutlined} from '@ant-design/icons';

const {TabPane} = Tabs;
const {TextArea} = Input;

const colorByLevel = (quantity) => {
    var color = '#262626'
    if (quantity > 0 && quantity <= 500) {
        color = '#851d41'
    } else if (quantity > 500 && quantity <= 2500) {
        color = '#df8543'
    } else if (quantity > 2500 && quantity <= 5000) {
        color = '#ecce6d'
    } else if (quantity > 5000 && quantity <= 10000) {
        color = '#4dd599'
    } else if (quantity > 10000) {
        color = '#3fc5f0'
    } else {
        color = '#262626'
    }
    return color
}
export const InfoPopup = ({
    visible,
    data,
    language,
    onGoogleMap,
    onCall,
    onCancel,
    user
}) => {
    var reportTxt =''
    const parseType = (type) => {
        var str
        switch (type) {
            case 1:
                str = localization()[language].n95mask;
                break;
            case 2:
                str = localization()[language].cottonMask;
                break;
            case 3:
                str = localization()[language].filter;
                break;
            case 4:
                str = localization()[language].alcGel;
                break;
            default:
                str = localization()[language].normalMask;
                break;
        }
        return str
    }
    const verify = async(data,user) => {
        if(user){
            sendAnalyze('verify')
            const res = await verifyShop(data.id,user.email)
            await createVerifyData(data.id,user.email)
            message[res.type](res.msg)
        }
    }

    return (
        <Modal
            title={data
            ? `${localization()[language].shopTxt}: ${data.name}`
            : ''}
            centered
            forceRender={true}
            visible={visible}
            onCancel={() => {
            onCancel('info')
        }}
            afterClose={() => {
            console.log('afterclose')
        }}
            footer={[(
                <Button
                    type="link"
                    danger
                    key="issue"
                    onClick={() => {
                        Modal.warning({title: 'Report Information',centered:true, content: (
                            <div>
                                <p style={{fontSize:12}}>{localization()[language].missingValuesTxt}</p>
                                <TextArea onChange={(e)=>{
                                    reportTxt = e.target.value
                                }} rows={4} />
                            </div>
                        ), async onOk() {
                            if(reportTxt !== ''){
                                sendAnalyze('report')
                                const res = await reportShop(data.id,reportTxt)
                                await createReportData(data.id,reportTxt)
                                message[res.type](res.msg)
                            }
                        }});
                    
                }}>
                    {localization()[language].issueReport}
                </Button>
            ), (
                <Button
                    key="back"
                    onClick={() => {
                    onCancel('info')
                }}>
                    Back
                </Button>
            ), (
                <Button
                    key="google"
                    onClick={() => {
                    onGoogleMap()
                }}>
                    {localization()[language].directionButton}
                </Button>
            ), (
                <Button
                    key="call"
                    type="primary"
                    onClick={() => {
                    onCall()
                }}>
                    {localization()[language].callButton}
                </Button>
            )]}>
            <Tabs
                defaultActiveKey="1"
                style={{
                marginTop: -24
            }}>
                <TabPane tab={localization()[language].stockInfo} key="1">
                    <Row gutter={16}>
                        <Col span={14}>
                            <Card size="small">
                                <Statistic
                                    title={localization()[language].shopQuantity}
                                    value={data
                                    ? data.quantity
                                    : ''}
                                    valueStyle={{
                                    fontWeight: 800,
                                    color: data
                                        ? colorByLevel(data.quantity)
                                        : '#282828'
                                }}/>
                            </Card>
                        </Col>
                        <Col span={10}>
                            <Card size="small">
                                <Statistic
                                    title={localization()[language].shopPrice}
                                    value={data
                                    ? `${data.price} ${localization()[language].priceUnit}`
                                    : ''}
                                    valueStyle={{
                                    fontWeight: 800
                                }}/>
                            </Card>
                        </Col>
                        <Col span={12}>
                            <img
                                style={{
                                padding: 18
                            }}
                                src={data
                                ? data.picUrl || ''
                                : ''}></img>
                        </Col>
                        <Col span={12}>
                            <p
                                style={{
                                marginTop: 12,
                                textAlign: 'right',
                                fontSize: 16,
                                fontWeight: 800,
                                color: '#006d75'
                            }}>{data
                                    ? `${parseType(data.type || 0)}`
                                    : ''}</p>
                            <p
                                style={{
                                marginTop: -10,
                                textAlign: 'right'
                            }}>{data
                                    ? `${localization()[language].callButton}. ${data.phone}`
                                    : ''}</p>
                            <div
                                style={{
                                textAlign: 'right',
                                marginTop: -10
                            }}>
                                <Rate character={< SafetyCertificateOutlined />} allowHalf disabled value={data && data.verifyFriends?data.verifyFriends.length:0}/>
                            </div>
                        </Col>
                    </Row>
                </TabPane>
                <TabPane tab={localization()[language].shopInfo} key="2">
                    <p>{`${localization()[language].shopName} : ${data
                            ? data.name
                            : ''}`}</p>
                    <p>{`${localization()[language].shopAddress} : ${data
                            ? data.address
                            : ''}`}</p>
                    <p>{`${localization()[language].shopPhone} : ${data
                            ? data.phone
                            : ''}`}</p>
                    <p>{`${localization()[language].shopDetail} : ${data
                            ? data.detail
                            : ''}`}</p>
                </TabPane>
                <TabPane tab={localization()[language].verifyBtn} key="3">
                    {user
                        ? (
                            <div>
                                <p>{`${localization()[language].verifyDetail}`}</p>
                                <Button
                                    key="google"
                                    onClick={() => {
                                    verify(data||null,user||null)
                                }}>
                                    {localization()[language].verifyBtn}
                                </Button>
                            </div>
                        )
                        : (
                            <p>{`${localization()[language].cannotVerify}`}</p>
                        )}

                </TabPane>
            </Tabs>
            <div
                style={{
                textAlign: 'right',
                marginTop: 8,
                marginBottom: -24
            }}>
                <p style={{
                    fontSize: 10
                }}>{`*${localization()[language]
                        .updateAtTxt} : ${moment(data
                        ? data.updatedAt
                        : '1970/1/1')
                        .format('DD/MM/YY HH:mm:ss')}`}</p>
            </div>
        </Modal>
    )
}