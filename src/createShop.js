import React, {Component} from 'react';
import {
    Modal,
    Input,
    Upload,
    Form,
    Select,
    Icon
} from "antd";
import {localization} from './Localization'
import {LoadingOutlined, CloudUploadOutlined} from '@ant-design/icons';

const {Option} = Select;
const {TextArea} = Input;

export const CreateShopModalForm = ({
    visible,
    language,
    onSubmit,
    onCancel,
    imageUploadUrl,
    onUpload,
    isUploadWaiting
}) => {
    const [form] = Form.useForm();
    const formItemLayout = {
        labelCol: {
            xs: {
                span: 24
            },
            sm: {
                span: 6
            }
        },
        wrapperCol: {
            xs: {
                span: 24
            },
            sm: {
                span: 12
            }
        }
    };
    const uploadButton = (
        <div>
        {isUploadWaiting?<LoadingOutlined />:<CloudUploadOutlined />}
            <div className="ant-upload-text">Upload</div>
        </div>
    );
    return (
        <Modal
            title={localization()[language].createNewShop}
            visible={visible}
            centered
            okText="Submit"
            afterClose={() => {
            console.log('afterclose');
            form.resetFields();
        }}
            onCancel={() => {
            form.resetFields();
            onCancel('create')
        }}
            onOk={() => {
            form
                .validateFields()
                .then(values => {
                    values.picUrl = imageUploadUrl
                    onSubmit(values);
                })
                .catch(info => {
                    console.log('Validate Failed:', info);
                });
        }}>
            <Form {...formItemLayout} form={form}>
                <Form.Item
                    label={localization()[language].shopName}
                    name="name"
                    rules={[{
                        required: true,
                        message: localization()[language].missingValuesTxt
                    }
                ]}>
                    <Input/>
                </Form.Item>
                <Form.Item
                    label={localization()[language].shopAddress}
                    name="address"
                    rules={[{
                        required: true,
                        message: localization()[language].missingValuesTxt
                    }
                ]}>
                    <Input/>
                </Form.Item>
                <Form.Item
                    label={localization()[language].shopPhone}
                    name="phone"
                    rules={[{
                        required: true,
                        message: localization()[language].missingValuesTxt
                    }
                ]}>
                    <Input/>
                </Form.Item>
                <Form.Item
                    label={localization()[language].shopProductType}
                    name="type"
                    rules={[{
                        required: true,
                        message: localization()[language].missingValuesTxt
                    }
                ]}>
                    <Select
                        defaultValue={0}>
                        <Option value={0}>{localization()[language].normalMask}</Option>
                        <Option value={1}>{localization()[language].n95mask}</Option>
                        <Option value={2}>{localization()[language].cottonMask}</Option>
                        <Option value={3}>{localization()[language].filter}</Option>
                        <Option value={4}>{localization()[language].alcGel}</Option>
                    </Select>
                </Form.Item>
                <Form.Item
                    label={localization()[language].shopQuantity}
                    name="quantity"
                    extra="ใส่จำนวนชิ้นทั้งหมด เป็นตัวเลข"
                    rules={[{
                        required: true,
                        message: localization()[language].missingValuesTxt
                    }
                ]}>
                    <Input/>
                </Form.Item>
                <Form.Item
                    label={localization()[language].shopPrice}
                    name="price"
                    extra="ใส่ราคาต่อชิ้น ไม่ต้องใส่หน่วย"
                    rules={[{
                        required: true,
                        message: localization()[language].missingValuesTxt
                    }
                ]}>
                    <Input/>
                </Form.Item>
                <Form.Item label={localization()[language].shopDetail} name="detail">
                    <TextArea rows={4}/>
                </Form.Item>
                <Form.Item label={localization()[language].shopProductPic} name="picUrl">
                    <Upload
                        name="avatar"
                        listType="picture-card"
                        className="avatar-uploader"
                        showUploadList={false}
                        action='https://www.mocky.io/v2/5cc8019d300000980a055e76'
                        onChange={(info) => {
                        onUpload(info)
                    }}>
                        {imageUploadUrl
                            ? <img
                                    src={imageUploadUrl}
                                    alt="avatar"
                                    style={{
                                    width: '100%'
                                }}/>
                            : uploadButton}
                    </Upload>

                </Form.Item>
            </Form>
        </Modal>
    )
}