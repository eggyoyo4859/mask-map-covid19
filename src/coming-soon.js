import React, {Component} from 'react';

export default class App extends Component {
    render() {
        return(
            <div>
                <h1 style={{marginTop:100,textAlign: 'center',fontSize:40}}>
                    ขออภัย... เวปปรับปรุงชั่วคราว
                </h1>
                <p style={{marginTop:24,textAlign: 'center',fontSize:32}}>
                เพื่อดำเนินการขอลดค่าใช้ด้าน Server
                </p>
                <p style={{marginTop:24,textAlign: 'center',fontSize:32}}>
                สามารถใช้งานได้ภายในวันที่ 4 เมษายน 2563
                </p>
            </div>
        )
    }
}