import React, {Component} from 'react';
import {
    Modal,
    Button,
    message,
    Input,
    Form,
    Tabs,
    Upload,
    Select
} from "antd";
import {LoadingOutlined, CloudUploadOutlined} from '@ant-design/icons';
import {localization} from './Localization'

const {Option} = Select;
const {TextArea} = Input;
const {TabPane} = Tabs;

export const EditShopModalForm = ({
    visible,
    data,
    language,
    onSubmit,
    onCancel,
    imageUploadUrl,
    onUpload,
    isUploadWaiting
}) => {

    const [form] = Form.useForm();
    form.setFieldsValue(data)
    if (data && data.picUrl && imageUploadUrl === '') {
        imageUploadUrl = data.picUrl
    }
    const formItemLayout = {
        labelCol: {
            xs: {
                span: 24
            },
            sm: {
                span: 6
            }
        },
        wrapperCol: {
            xs: {
                span: 24
            },
            sm: {
                span: 12
            }
        }
    };
    const uploadButton = (
        <div>
            {isUploadWaiting
                ? <LoadingOutlined/>
                : <CloudUploadOutlined/>}
            <div className="ant-upload-text">Upload</div>
        </div>
    );
    return (
        <Modal
            title={localization()[language].updateShopInfo}
            centered
            forceRender={true}
            visible={visible}
            okText="Submit"
            onCancel={() => {
            onCancel('edit');
            form.resetFields();
        }}
            onOk={() => {
            form
                .validateFields()
                .then(values => {
                    values.picUrl = imageUploadUrl;
                    onSubmit(values);
                })
                .catch(info => {
                    console.log('Validate Failed:', info);
                });
        }}>
            <Form {...formItemLayout} form={form}>
                <Form.Item
                    label={localization()[language].shopId}
                    name="id"
                    rules={[{
                        required: true
                    }
                ]}>
                    <Input
                        value={data
                        ? data.id
                        : ''}
                        disabled={true}/>
                </Form.Item>
                <Tabs defaultActiveKey="1">
                    <TabPane tab={localization()[language].stockInfo} key="1">
                        <Form.Item
                            label={localization()[language].shopProductType}
                            name="type"
                            rules={[{
                                required: true,
                                message: localization()[language].missingValuesTxt
                            }
                        ]}>
                            <Select defaultValue={0}>
                                <Option value={0}>{localization()[language].normalMask}</Option>
                                <Option value={1}>{localization()[language].n95mask}</Option>
                                <Option value={2}>{localization()[language].cottonMask}</Option>
                                <Option value={3}>{localization()[language].filter}</Option>
                                <Option value={4}>{localization()[language].alcGel}</Option>
                            </Select>
                        </Form.Item>
                        <Form.Item
                            label={localization()[language].shopQuantity}
                            name="quantity"
                            rules={[{
                                required: true,
                                message: localization()[language].missingValuesTxt
                            }
                        ]}>
                            <Input/>
                        </Form.Item>
                        <Form.Item
                            label={localization()[language].shopPrice}
                            name="price"
                            rules={[{
                                required: true,
                                message: localization()[language].missingValuesTxt
                            }
                        ]}>
                            <Input/>
                        </Form.Item>
                    </TabPane>
                    <TabPane tab={localization()[language].shopInfo} key="2">
                        <Form.Item
                            label={localization()[language].shopName}
                            name="name"
                            rules={[{
                                required: true,
                                message: localization()[language].missingValuesTxt
                            }
                        ]}>
                            <Input/>
                        </Form.Item>
                        <Form.Item
                            label={localization()[language].shopAddress}
                            name="address"
                            rules={[{
                                required: true,
                                message: localization()[language].missingValuesTxt
                            }
                        ]}>
                            <Input/>
                        </Form.Item>
                        <Form.Item
                            label={localization()[language].shopPhone}
                            name="phone"
                            rules={[{
                                required: true,
                                message: localization()[language].missingValuesTxt
                            }
                        ]}>
                            <Input/>
                        </Form.Item>
                        <Form.Item label={localization()[language].shopDetail} name="detail">
                            <TextArea rows={4}/>
                        </Form.Item>
                        <Form.Item label={localization()[language].shopProductPic} name="picUrl">
                            <Upload
                                name="avatar"
                                listType="picture-card"
                                className="avatar-uploader"
                                showUploadList={false}
                                action='https://www.mocky.io/v2/5cc8019d300000980a055e76'
                                onChange={(info) => {
                                onUpload(info)
                            }}>
                                {imageUploadUrl
                                    ? <img
                                            src={imageUploadUrl}
                                            alt="avatar"
                                            style={{
                                            width: '100%'
                                        }}/>
                                    : uploadButton}
                            </Upload>
                        </Form.Item>
                    </TabPane>
                    <TabPane tab={localization()[language].locationInfo} key="3">
                        <Form.Item
                            label="Latitude"
                            name="lat"
                            rules={[{
                                required: true,
                                message: localization()[language].missingValuesTxt
                            }
                        ]}>
                            <Input/>
                        </Form.Item>
                        <Form.Item
                            label="Longitude"
                            name="lng"
                            rules={[{
                                required: true,
                                message: localization()[language].missingValuesTxt
                            }
                        ]}>
                            <Input/>
                        </Form.Item>
                    </TabPane>
                </Tabs>
            </Form>
        </Modal>
    )
}