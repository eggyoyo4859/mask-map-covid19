import * as firebase from 'firebase';
import 'firebase/firestore';
import "firebase/auth";
import "firebase/storage";
import "firebase/analytics";
import shortid from 'shortid'

import geohash from 'ngeohash'
import {getDistance} from 'geolib';

const firebaseConfig = {
    apiKey: "AIzaSyCAG_V3Ex66eGtPO037yqxw2KxdXfxVnEE",
    authDomain: "mask-map-covid19.firebaseapp.com",
    databaseURL: "https://mask-map-covid19.firebaseio.com",
    projectId: "mask-map-covid19",
    storageBucket: "mask-map-covid19.appspot.com",
    messagingSenderId: "70014802145",
    appId: "1:70014802145:web:8f455483fbeaef0953bfa0",
    measurementId: "G-QP9BK4NK7N"
};
firebase.initializeApp(firebaseConfig);
var db = firebase.firestore()
const firebaseAuth = firebase.auth;
var storage = firebase.storage();
export const sendAnalyze = (event) => {
    firebase.analytics().logEvent(event);
}
export const getDataByID = async(id) => {
    const promise = new Promise((resolve, reject) => {
        db
            .collection("shop")
            .doc(id)
            .get()
            .then(function (doc) {
                if (doc.exists) {
                    console.log("Document data:", doc.data());
                    resolve({
                        type: 'success',
                        data: doc.data()
                    })
                } else {
                    console.log("No such document!");
                    resolve({type: 'error', msg: 'เกิดข้อผิดพลาดระหว่างการดึงข้อมูล'})
                }
            })
            .catch(function (error) {
                console.log("Error getting document:", error);
                resolve({type: 'error', msg: 'เกิดข้อผิดพลาดระหว่างการดึงข้อมูล'})
            });
    })
    return promise
}
export const editDataByID = async(data) => {
    const promise = new Promise((resolve, reject) => {
        db
            .collection("shop")
            .doc(data.id)
            .update(data)
            .then(function () {
                resolve({type: 'success', msg: 'บันทึกข้อมูลเรียบร้อย', data: data})
            })
            .catch(function (error) {
                console.log("Error getting documents: ", error);
                resolve({type: 'error', msg: 'เกิดข้อผิดพลาดระหว่างการดึงข้อมูล'})
            });
    })
    return promise
}

export const isAuthenticated = async() => {
    const promise = new Promise((resolve, reject) => {
        firebaseAuth()
            .onAuthStateChanged(function (user) {
                if (user) {
                    resolve({type: 'success', user: user})
                } else {
                    resolve({type: 'error', msg: 'No user found!'})
                }
            });
    })
    return promise
}

export const loginWithFacebook = async() => {
    const promise = new Promise((resolve, reject) => {
        var provider = new firebaseAuth.FacebookAuthProvider();
        firebaseAuth()
            .signInWithPopup(provider)
            .then(function (result) {
                if (result.user) {
                    //var token = result.credential.accessToken;
                    var user = result.user;
                    resolve({type: 'success', msg: 'Log in success!', user: user})
                } else {
                    resolve({type: 'error', msg: 'No user found!'})
                }
            })
            .catch(function (error) {
                var errorMessage = error.message;
                console.log(`error:${error}`)
                resolve({type: 'error', msg: errorMessage})
            })
    })
    return promise
}

export const loginWithFacebookRedirect = async() => {
    const promise = new Promise((resolve, reject) => {
        var provider = new firebaseAuth.FacebookAuthProvider();
        firebaseAuth().signInWithRedirect(provider);

        firebaseAuth()
            .getRedirectResult()
            .then(function (result) {
                if (result.user) {
                    //var token = result.credential.accessToken;
                    var user = result.user;
                    resolve({type: 'success', msg: 'Log in success!', user: user})
                } else {
                    resolve({type: 'error', msg: 'No user found!'})
                }
            })
            .catch(function (error) {
                var errorMessage = error.message;
                console.log(`error:${error}`)
                resolve({type: 'error', msg: errorMessage})
            });
    })
    return promise
}

export const loginWithGoogle = async() => {
    const promise = new Promise((resolve, reject) => {
        var provider = new firebaseAuth.GoogleAuthProvider();
        firebaseAuth()
            .signInWithPopup(provider)
            .then(function (result) {
                if (result.user) {
                    //var token = result.credential.accessToken;
                    var user = result.user;
                    resolve({type: 'success', msg: 'Log in success!', user: user})
                } else {
                    resolve({type: 'error', msg: 'No user found!'})
                }
            })
            .catch(function (error) {
                //var errorCode = error.code;
                var errorMessage = error.message;
                //var email = error.email; var credential = error.credential;
                console.log(`error:${error}`)
                resolve({type: 'error', msg: errorMessage})
            })

    })
    return promise
}

export const logout = async() => {
    const promise = new Promise((resolve, reject) => {
        firebaseAuth()
            .signOut()
            .then(function () {
                console.log('Signed Out');
                resolve({type: 'success', msg: 'Log out success!'})
            }, function (error) {
                console.error('Sign Out Error', error);
                resolve({type: 'error', msg: 'เกิดข้อผิดพลาดระหว่างการ Log out'})
            });
    });
    return promise
}

export const getAllShop = async(uid) => {
    const promise = new Promise((resolve, reject) => {
        var shops = []
        var query = db.collection('shop')
        if (uid) {
            query = query.where('owner', '==', uid)
        }
        query
            .get()
            .then(async function (querySnapshot) {
                if (querySnapshot.size > 0) {
                    querySnapshot
                        .forEach(function (doc) {
                            const data = doc.data()
                            shops.push(data)
                        });
                    resolve({type: 'success', results: shops, count: shops.length})
                } else {
                    resolve({type: 'error', msg: 'ข้อมูลยังไม่มีในระบบ'})
                }
            })
            .catch(function (error) {
                console.log("Error getting documents: ", error);
                resolve({type: 'error', msg: 'ข้อมูลยังไม่มีในระบบ'})
            });
    })
    return promise
}

export const createNewShopData = async(data) => {
    const promise = new Promise((resolve, reject) => {
        db
            .collection("shop")
            .doc(data.id)
            .set(data, {merge: true})
            .then(function () {
                resolve({type: 'success', msg: 'บันทึกข้อมูลเรียบร้อย!'})
            })
            .catch(function (error) {
                console.log("Error getting documents: ", error);
                resolve({type: 'error', msg: 'เกิดข้อผิดพลาดระหว่างการบันทึก กรุณาตรวจสอบข้อมูลและลองอีกครั้ง'})
            });
    })
    return promise
}

export const parseGeoPoint = (loc) => {
    return new firebase
        .firestore
        .GeoPoint(loc.lat, loc.lng)
}

const getGeohashRange = (latitude, longitude, distance,) => {
    const lat = 0.0144927536231884; // degrees latitude per mile
    const lon = 0.0181818181818182; // degrees longitude per mile
    const mile = 1.609344;
    const lowerLat = latitude - lat * distance * mile;
    const lowerLon = longitude - lon * distance * mile;

    const upperLat = latitude + lat * distance * mile;
    const upperLon = longitude + lon * distance * mile;

    const lower = geohash.encode(lowerLat, lowerLon);
    const upper = geohash.encode(upperLat, upperLon);

    return {lower, upper};
};

export const findShopNearMe = async(latitude, longitude) => {
    const promise = new Promise((resolve, reject) => {
        const range = getGeohashRange(latitude, longitude, 100);
        console.log(`range : ${JSON.stringify(range)}`);
        var shops;

        db
            .collection("shop")
            .where("geoHash", ">=", range.lower)
            .where("geoHash", "<=", range.upper)
            .orderBy('geoHash', 'desc')
            .get()
            .then(function (querySnapshot) {
                if (querySnapshot.size > 0) {
                    querySnapshot
                        .forEach(function (doc) {
                            const data = doc.data()
                            const dis = getDistance({
                                latitude: latitude,
                                longitude: longitude
                            }, {
                                latitude: data.geo.F,
                                longitude: data.geo.V
                            });
                            if (!shops) {
                                shops = data
                            } else {
                                const shopDis = getDistance({
                                    latitude: latitude,
                                    longitude: longitude
                                }, {
                                    latitude: shops.geo.F,
                                    longitude: shops.geo.V
                                });
                                if (shopDis > dis) {
                                    shops = data
                                }
                            }
                        });
                    resolve({type: 'success', results: shops, count: shops.length})
                } else {
                    resolve({type: 'error', msg: 'ข้อมูลยังไม่มีในระบบ'})
                }

            })
            .catch(function (error) {
                console.log("Error getting documents: ", error);
                resolve({type: 'error', msg: 'เกิดข้อผิดพลาดระหว่างการดึงข้อมูล'})

            });

    })
    return promise
}

export const getTop3LowPriceShop = async() => {
    const promise = new Promise((resolve, reject) => {
        var shops = []
        var query = db
            .collection('shop')
            .where('price', '>', 0)
            .orderBy('price')
            .limit(3)

        query
            .get()
            .then(async function (querySnapshot) {
                if (querySnapshot.size > 0) {
                    querySnapshot
                        .forEach(function (doc) {
                            const data = doc.data()
                            shops.push(data)
                        });
                    resolve({type: 'success', results: shops, count: shops.length})
                } else {
                    resolve({type: 'error', msg: 'ข้อมูลยังไม่มีในระบบ'})
                }
            })
            .catch(function (error) {
                console.log("Error getting documents: ", error);
                resolve({type: 'error', msg: 'ข้อมูลยังไม่มีในระบบ'})
            });
    })
    return promise
}

export const getTop3MoreStockShop = async() => {
    const promise = new Promise((resolve, reject) => {
        var shops = []
        var query = db
            .collection('shop')
            .orderBy('quantity', 'desc')
            .limit(3)
        query
            .get()
            .then(async function (querySnapshot) {
                if (querySnapshot.size > 0) {
                    querySnapshot
                        .forEach(function (doc) {
                            const data = doc.data()
                            shops.push(data)
                        });
                    resolve({type: 'success', results: shops, count: shops.length})
                } else {
                    resolve({type: 'error', msg: 'ข้อมูลยังไม่มีในระบบ'})
                }
            })
            .catch(function (error) {
                console.log("Error getting documents: ", error);
                resolve({type: 'error', msg: 'ข้อมูลยังไม่มีในระบบ'})
            });
    })
    return promise
}

export const getSponsor = async() => {
    const promise = new Promise((resolve, reject) => {
        var sponsors = []
        var query = db
            .collection('donator')
            .orderBy('donateDate', 'desc')
        query
            .get()
            .then(async function (querySnapshot) {
                if (querySnapshot.size > 0) {
                    querySnapshot
                        .forEach(function (doc) {
                            const data = doc.data()
                            sponsors.push(data)
                        });
                    resolve({type: 'success', results: sponsors, count: sponsors.length})
                } else {
                    resolve({type: 'error', msg: 'ข้อมูลยังไม่มีในระบบ'})
                }
            })
            .catch(function (error) {
                console.log("Error getting documents: ", error);
                resolve({type: 'error', msg: 'ข้อมูลยังไม่มีในระบบ'})
            });
    })
    return promise
}

export const getVIPSponsor = async() => {
    const promise = new Promise((resolve, reject) => {
        var sponsors = []
        var query = db
            .collection('donator').where('isVIP','==',true)
        query
            .get()
            .then(async function (querySnapshot) {
                if (querySnapshot.size > 0) {
                    querySnapshot
                        .forEach(function (doc) {
                            const data = doc.data()
                            sponsors.push(data)
                        });
                    resolve({type: 'success', results: sponsors, count: sponsors.length})
                } else {
                    resolve({type: 'error', msg: 'ข้อมูลยังไม่มีในระบบ'})
                }
            })
            .catch(function (error) {
                console.log("Error getting documents: ", error);
                resolve({type: 'error', msg: 'ข้อมูลยังไม่มีในระบบ'})
            });
    })
    return promise
}

export const getFund = async() => {
    const promise = new Promise((resolve, reject) => {
        var query = db
            .collection('config')
            .doc('fund')
        query
            .get()
            .then(function (doc) {
                resolve({
                    type: 'success',
                    data: doc.data()
                })
            })
            .catch(function (error) {
                console.log("Error getting documents: ", error);
                resolve({type: 'error', msg: 'ข้อมูลยังไม่มีในระบบ'})
            });
    })
    return promise
}

export const uploadPic = async(file) => {
    const promise = new Promise((resolve, reject) => {
        var storageRef = storage
            .ref()
            .child('productImage/'+file.file.name);
        storageRef
            .put(file.file.originFileObj)
            .then(function (snapshot) {
                snapshot.ref.getDownloadURL().then(function(downloadURL) {
                    resolve({
                        type: 'success',
                        data: downloadURL
                    })
                  });                
            })
            .catch(function (error) {
                console.log("Error getting documents: ", error);
                resolve({type: 'error', msg: 'ข้อมูลยังไม่มีในระบบ'})
            });
    })
    return promise
}

export const verifyShop = async(data,user) => {
    const promise = new Promise((resolve, reject) => {
        db
            .collection("shop")
            .doc(data)
            .set({
                verifyFriends:firebase.firestore.FieldValue.arrayUnion(user)
            }, { merge: true })
            .then(function () {
                resolve({type: 'success', msg: 'บันทึกข้อมูลเรียบร้อย!'})
            })
            .catch(function (error) {
                console.log("Error getting documents: ", error);
                resolve({type: 'error', msg: 'เกิดข้อผิดพลาดระหว่างการบันทึก กรุณาตรวจสอบข้อมูลและลองอีกครั้ง'})
            });
    })
    return promise
}

export const reportShop = async(data,report) => {
    const promise = new Promise((resolve, reject) => {
        db
            .collection("shop")
            .doc(data)
            .set({
                report:firebase.firestore.FieldValue.arrayUnion(report)
            }, { merge: true })
            .then(function () {
                resolve({type: 'success', msg: 'บันทึกข้อมูลเรียบร้อย!'})
            })
            .catch(function (error) {
                console.log("Error getting documents: ", error);
                resolve({type: 'error', msg: 'เกิดข้อผิดพลาดระหว่างการบันทึก กรุณาตรวจสอบข้อมูลและลองอีกครั้ง'})
            });
    })
    return promise
}

export const createReportData = async(dataId,msg) => {
    const promise = new Promise((resolve, reject) => {
        db
            .collection("report")
            .doc(shortid.generate())
            .set({dataId:dataId,msg:msg})
            .then(function () {
                resolve({type: 'success', msg: 'บันทึกข้อมูลเรียบร้อย!'})
            })
            .catch(function (error) {
                console.log("Error getting documents: ", error);
                resolve({type: 'error', msg: 'เกิดข้อผิดพลาดระหว่างการบันทึก กรุณาตรวจสอบข้อมูลและลองอีกครั้ง'})
            });
    })
    return promise
}

export const createVerifyData = async(dataId,email) => {
    const promise = new Promise((resolve, reject) => {
        db
            .collection("verify")
            .doc(shortid.generate())
            .set({dataId:dataId,userEmail:email})
            .then(function () {
                resolve({type: 'success', msg: 'บันทึกข้อมูลเรียบร้อย!'})
            })
            .catch(function (error) {
                console.log("Error getting documents: ", error);
                resolve({type: 'error', msg: 'เกิดข้อผิดพลาดระหว่างการบันทึก กรุณาตรวจสอบข้อมูลและลองอีกครั้ง'})
            });
    })
    return promise
}