import React, {Component} from 'react';
import {Drawer, List, Button, Modal,Progress} from "antd";
import {localization} from './Localization'
import {CoffeeOutlined, SmileOutlined} from '@ant-design/icons';
import {sendAnalyze} from './firebase'

export const SponsorDrawer = ({
    visible,
    data,
    language,
    onCancel,
    onSponsor,
    fund,
    cost
}) => {
    return (
        <Drawer
            title={localization()[language].sponsorTxt}
            placement='left'
            width={280}
            closable={false}
            onClose={() => {
            onCancel()
        }}
            visible={visible}
            footer={(
            <div style={{
                textAlign: 'center'
            }}>
            
                <p style={{
                    marginBottom: -5,
                    fontSize:11
                }}>{`${localization()[language].donateResultTxt} / ${localization()[language].costResultTxt} : ${fund} / ${cost}`}</p>
                <Progress style={{
                    marginBottom: 12
                }} percent={(fund/cost)*100} showInfo={false} />
                <Button
                    block
                    icon={< SmileOutlined />}
                    type="default"
                    style={{
                    marginBottom: 12
                }}
                    onClick={() => {
                    onSponsor()
                }}>{localization()[language].bigSponsorTxt}</Button>
                <Button
                    block
                    icon={< CoffeeOutlined />}
                    type="primary"
                    onClick={() => {
                        sendAnalyze('press_donate')
                         Modal.info({title: 'Thank you!!',centered:true, content: (
                            <div>
                                <p style={{fontSize:12}}>{localization()[language].sponsorDetailTxt}</p>
                                <img src="https://firebasestorage.googleapis.com/v0/b/mask-map-covid19.appspot.com/o/592100A3-FE66-46E6-BE12-5616D3A79346.jpg?alt=media&token=151ff72a-de54-4476-b3cf-aee10f953bdb" />
                            </div>
                        ), onOk() {}});
                }}>{localization()[language].joinSponsor}</Button>
            </div>
        )}>
            <List
                grid={{
                gutter: 16,
                xs: 2,
                sm: 2,
                md: 2,
                lg: 2,
                xl: 2,
                xxl: 2
            }}
                size="small"
                style={{
                textAlign: 'center'
            }}
                dataSource={data}
                renderItem={item => <List.Item>
                <img
                    src={item[language==='th'?'picUrl':'picUrl-en'] || item.picUrl}
                    onClick={() => {
                        window.location.href=item.url
                }}></img>
                <a href={item.url} target="_blank">
                    {item[language==='th'?'name':'name-en'] || item.name}
                </a>
            </List.Item>}/>
        </Drawer>
    )
}