import React, {Component} from 'react';
import * as firebase from 'firebase';
import 'firebase/firestore';
import mapboxgl from 'mapbox-gl'
import {MenuOutlined,QuestionCircleOutlined} from '@ant-design/icons';
import {
    Drawer,
    Button,
    Select,
    Modal,
    List,
    Form,
    Input,
    Tabs,
    Spin,
    Radio,
    notification,
    message,
    Carousel
} from 'antd';
import {EditShopModalForm} from "./editShop";
import {CreateShopModalForm} from "./createShop";
import { SponsorDrawer } from "./sponsorDrawer";
import {InfoPopup} from "./infoPopup";
import {localization} from './Localization'
import shortid from 'shortid'
import './App.css';
import {
    editDataByID,
    isAuthenticated,
    loginWithFacebook,
    loginWithGoogle,
    logout,
    getAllShop,
    createNewShopData,
    parseGeoPoint,
    getDataByID,
    findShopNearMe,
    getTop3LowPriceShop,
    getTop3MoreStockShop,
    loginWithFacebookRedirect,
    getSponsor,
    getVIPSponsor,
    getFund,
    uploadPic,
    sendAnalyze
} from './firebase'
import geohash from 'ngeohash'
import {ReactComponent as LegendPic} from './mask-legend.png'

const {Option} = Select;
const {TabPane} = Tabs;
var db = firebase.firestore()

mapboxgl.accessToken = 'pk.eyJ1IjoiZWdneW80ODU5IiwiYSI6ImNpb2wzaHFkYTAwM291MG0wNWZ4cW5sMGcifQ.yVv_6LIdOz' +
        '8bp5ky9YDAtw'
const legendConfig = {
          message: 'ความหมายสัญลักษณ์',
          description: (
            <div style={{textAlign:'center'}}>
              <img src="./mask-legend.png"/>
              <p style={{fontSize:12}}>ตัวเลขในไอคอน คือ ราคา</p>
            </div>
          ),
          placement:'bottomLeft',
          style: {
            width: 180,
            marginLeft:-16
          },
        };
const colorByLevel = (quantity) => {
    var color = '#262626'
    if (quantity > 0 && quantity <= 500) {
        color = '#851d41'
    } else if (quantity > 500 && quantity <= 2500) {
        color = '#df8543'
    } else if (quantity > 2500 && quantity <= 5000) {
        color = '#ecce6d'
    } else if (quantity > 5000 && quantity <= 10000) {
        color = '#4dd599'
    } else if (quantity > 10000) {
        color = '#3fc5f0'
    } else {
        color = '#262626'
    }
    return color
}

function getBase64(img, callback) {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
}

function beforeUpload(file) {
    const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
    if (!isJpgOrPng) {
        message.error('You can only upload JPG/PNG file!');
    }
    const isLt3M = file.size / 1024 / 1024 < 3;
    if (!isLt3M) {
        message.error('Image must smaller than 3MB!');
    }
    return isJpgOrPng && isLt3M;
}
export default class App extends Component {
    mapRef = React.createRef();
    map;
    centerMarker;
    unsubscribe;


    constructor(props, context) {
        super(props, context)
        this.state = {
            isFindBtnLoading:false,
            isMainLoad: true,
            isDrawerVisible: false,
            isCreateModalVisible: false,
            isEditModalVisible: false,
            isInfoModalVisible: false,
            isSponsorDrawerVisible:false,
            isEditLoading: false,
            isAuth: true,
            currentUser: null,
            isAnonymousMode: false,
            markerGeoPoint: null,
            myShopData: [],
            selectedMarker:null,
            data: null,
            top3StockData:[],
            top3PriceData:[],
            language:'th',
            sponsorData:[],
            vipSponsorData:[],
            fund:3000,
            cost:7000,
            imageUploadUrl:'',
            isUploadWaiting:false,
            productFilterType:null,
            vipSponsorIndex:0,
        }
    }
    componentDidMount = async() => {
        this.checkAuthenticate()
        this.getSponsorDate()
        this.getVIPSponsorDate()
        this.getFundData()
        sendAnalyze('open_app')
    }
    checkAuthenticate = async() => {
        const res = await isAuthenticated()
        console.log(JSON.stringify(res.user))

        this.initMap()
        this.makeCenterMarker()

        if (res.user) {
            this.setState({
                isAuth: true,
                currentUser: res.user,
                isMainLoad: false
            }, () => {
                this.listenShopData(res.user.uid)
                this.addMarkerCenter()
                notification.open(legendConfig);

            })
        } else {
            this.listenShopData()
            this.setState({isAuth: false, isMainLoad: false})
            notification.open(legendConfig);

        }

    }
    listenShopData = async(uid) => {
        const that = this
        var query = db.collection("shop")
        if (uid) {
            query = query.where('owner', '==', uid)
        }
        if(this.state.productFilterType){
            query = query.where('type', '==', this.state.productFilterType)
        }
        this.unsubscribe = query.onSnapshot(async function (snapshot) {
            that.getMyShop(uid)
            that.getTop3Price()
            that.getTop3Stock()
            snapshot
                .docChanges()
                .forEach(function (change) {
                    const data = change
                        .doc
                        .data();
                    if (change.type === "added") {
                        console.log("New shop: ", data);
                        new mapboxgl
                            .Marker(that.renderMarkerDOM(data))
                            .setLngLat([data.geo.V, data.geo.F])
                            .addTo(that.map);
                    }
                    if (change.type === "modified") {
                        console.log("Modified shop: ", data);
                        const element = document.getElementById(data.id)
                        element
                            .parentNode
                            .removeChild(element);
                        new mapboxgl
                            .Marker(that.renderMarkerDOM(data))
                            .setLngLat([data.geo.V, data.geo.F])
                            .addTo(that.map);
                    }
                    if (change.type === "removed") {
                        console.log("Removed shop: ", data);
                        const element = document.getElementById(data.id)
                        element
                            .parentNode
                            .removeChild(element);
                    }
                });
        });
    }
    handleUploadChange = async info => {
        console.log(`info ${JSON.stringify(info)}`)

        if (info.file.status === 'uploading') {
            this.setState({isUploadWaiting: true});
            return;
        }
        if (info.file.status === 'done') {
            // Get this url from response in real world.
            const res = await uploadPic(info)
            console.log(`res; ${JSON.stringify(res)}`)
            if(res.type === 'success'){
                this.setState({imageUploadUrl:res.data, isUploadWaiting: false})
            }
            //getBase64(info.file.originFileObj, imageUrl => this.setState({imageUploadUrl:imageUrl, isUploadWaiting: false}),);
        }
    };
    getFundData = async() => {
        const res = await getFund()
        this.setState({fund:res.data.value,cost:res.data.cost})

    }
    getSponsorDate = async()=>{
        const res = await getSponsor()
        this.setState({sponsorData:res.results})
    }
    getVIPSponsorDate = async()=>{
        const res = await getVIPSponsor()
        this.setState({vipSponsorData:res.results})
        setInterval(() => {
            console.log('test')
            var i = this.state.vipSponsorIndex
            i === this.state.vipSponsorData.length - 1 ? i = 0:i+=1
            this.setState({vipSponsorIndex:i})
        }, 10000)
    }

    getMyShop = async(uid) => {
        const res = await getAllShop(uid)
        if (res.type === 'success') {
            this.setState({myShopData: res.results})
        } else {
            this.showMessage(res.type, res.msg)
        }
    }
    clearAllMarker = () => {
        var paras = document.getElementsByClassName('shop-marker');
        while (paras[0]) {
            paras[0]
                .parentNode
                .removeChild(paras[0]);
        }
    }
    getTop3Price = async() => {
      const res = await getTop3LowPriceShop()
      if(res.results){
        this.setState({top3PriceData:res.results})
      }
    }
    getTop3Stock = async() => {
      const res = await getTop3MoreStockShop()
      if(res.results){
        this.setState({top3StockData:res.results})
      }
    }

    renderMarkerDOM = (data) => {
      const that = this
        var el = document.createElement('div');
        el.id = data.id
        el.className = 'shop-marker';
        el.style.backgroundColor = colorByLevel(data.quantity)
        var fontSize = 15
        var marginTop = 3
        if (`${data.price}`.length >= 3) {
            fontSize = 12
            marginTop = 4
        }
        el.innerHTML = `<p style='width:100%;color:white;font-weight:bold;font-size:${fontSize}px;margin-top:${marginTop}px'>${data.price}</p>`

        el.addEventListener('click', function () {
          that.setState({selectedMarker:data},()=>{
            that.showModal('info')
          })
            
        });
        return el
    }

    initMap = () => {
        this.map = new mapboxgl.Map({
            container: this.mapRef.current,
            style: 'mapbox://styles/mapbox/light-v10',
            center: [
                100.523186, 13.736717
            ],
            zoom: 6
        });
        const onMoveEnd = () => {
          const center = this.map.getCenter()
          this.centerMarker.setLngLat([center.lng, center.lat])
          this.setState({markerGeoPoint: center})

          }
        this.map.on('move',onMoveEnd)
        this
            .map
            .addControl(new mapboxgl.GeolocateControl({
                positionOptions: {
                    enableHighAccuracy: true
                },
                trackUserLocation: true
            }));


        
    }

    makeCenterMarker = ()=> {
      console.log(this.map.getCenter())
      const center = this.map.getCenter()
      this.centerMarker = new mapboxgl
      .Marker({draggable: false})
      .setLngLat([center.lng, center.lat])
      const onDragEnd = () => {
      var lngLat = this
          .centerMarker
          .getLngLat();
      this.setState({markerGeoPoint: lngLat})
      console.log(`lngLat:${JSON.stringify(lngLat)}`)
      }
      this
      .centerMarker
      .on('dragend', onDragEnd)
    }
    addMarkerCenter(){
      const center = this.map.getCenter()
      this.centerMarker.setLngLat([center.lng, center.lat]).addTo(this.map);
    }
    removeMarkerCenter(){
      this.centerMarker.remove()
    }

    showMessage = (type, msg) => {
        if (type === 'success') {
            message.success(msg)
        } else if (type === 'error') {
            message.error(msg)
        } else {
            message.warning(msg)
        }
    }
    flyToId= async(id)=> {
      const res = await getDataByID(id)
      if(res.data){
        this.map.flyTo({
          center: [res.data.geo.V,res.data.geo.F],
          essential: true ,
          zoom: 12,
          });
      }
      
    }

    onShowDrawer = () => {
        this.setState({isDrawerVisible: true});
    };
    onDrawerClose = () => {
        this.setState({isDrawerVisible: false});
    };
    onSearchChange = (value) => {
        this.onDrawerClose()
        this.flyToId(value)
        console.log(`selected ${value}`);
    }

    onSearchBlur = () => {
        console.log('blur');
    }

    onSearchFocus = () => {
        console.log('focus');
    }

    onSearch = (val) => {
        console.log('search:', val);
    }

    showModal = (type) => {
        if (type === 'create') {
            this.setState({isCreateModalVisible: true});
        } else if(type === 'edit'){
            this.setState({isEditModalVisible: true});
            sendAnalyze('open_edit')
        } else {
            this.setState({isInfoModalVisible: true})
            sendAnalyze('open_info')
        }
    };

    handleEditModalOk = async(e) => {
        console.log(`e:${JSON.stringify(e)}`);
        e.updatedAt = new Date().getTime()
        e.quantity = parseInt(e.quantity)
        e.price = parseFloat(e.price)
        if(e.lat){
            e.geo = parseGeoPoint({lat:parseFloat(e.lat),lng:parseFloat(e.lng)})
            e.geoHash = geohash.encode(e.lat, e.lng)
        }
        const res = await editDataByID(e)
        this.showMessage(res.type, res.msg)
        this.setState({isEditModalVisible: false,imageUploadUrl:''});
        sendAnalyze('edited_data')
    };

    handleCreateModalOk = async(e) => {
        console.log(`e:${JSON.stringify(e)}`);
        e.createdAt = new Date().getTime()
        e.updatedAt = new Date().getTime()
        e.owner = this.state.currentUser.uid
        e.id = shortid.generate()
        e.geo = parseGeoPoint(this.state.markerGeoPoint)
        e.geoHash = geohash.encode(this.state.markerGeoPoint.lat, this.state.markerGeoPoint.lng)
        e.quantity = parseInt(e.quantity)
        e.price = parseFloat(e.price)
        const res = await createNewShopData(e)
        this.showMessage(res.type, res.msg)
        this.setState({isCreateModalVisible: false,imageUploadUrl:''});
        sendAnalyze('created_data')
    };

    handleModalCancel = (type) => {
        if (type === 'create') {
            this.setState({isCreateModalVisible: false});
        } else if(type === 'edit'){
            this.setState({isEditModalVisible: false});
        }else{
          this.setState({isInfoModalVisible: false})
        }
    };
    onLanguageChange = e => {
        console.log('radio checked', e.target.value);
        this.setState({
          language: e.target.value,
        });
        sendAnalyze('change_language')
      };

    googleAuth = async() => {
        const res = await loginWithGoogle()
        if (res.user) {
            this.setState({
                currentUser: res.user,
                isAuth: true
            }, () => {
                this.editorMode()
            })
            this.editorMode()
        }
        this.showMessage(res.type, res.msg)
    }

    facebookAuth = async() => {
        await loginWithFacebookRedirect()
        // const res = await loginWithFacebookRedirect()
        // if (res.user) {
        //     this.setState({
        //         currentUser: res.user,
        //         isAuth: true
        //     }, () => {
        //         this.editorMode()
        //     })
        // }
        // this.showMessage(res.type, res.msg)
    }
    handleLogout = async() => {
        const res = await logout()
        if (res.type === 'success') {
            this.setState({
                currentUser: null,
                isAuth: false
            }, () => {
                this.anonymousMode()
            })
        }
        this.showMessage(res.type, res.msg)
    }

    editorMode = async() => {
        this.setState({isAnonymousMode: false,myShopData:[]})
        this.clearAllMarker()
        this.addMarkerCenter()
        this.unsubscribe()
        this.listenShopData(this.state.currentUser.uid)
    }
    anonymousMode = async() => {
        this.setState({isAnonymousMode: true,myShopData:[]})
        this.clearAllMarker()
        this.removeMarkerCenter()
        this.unsubscribe()
        this.listenShopData()
    }

    handleGoogleMap =()=> {
      const {selectedMarker} = this.state
      window.open(`https://maps.google.com/?q=${selectedMarker
        ? selectedMarker.geo.F + ',' + selectedMarker.geo.V
        : ''}`,'_blank')
        sendAnalyze('press_google')

    }

    handleCall=() => {
      const {selectedMarker} = this.state
      window.location.href = `tel:${selectedMarker
        ? selectedMarker.phone
        : ''}`;
        sendAnalyze('press_call')

    }
    handleSponsorCancel =() => {
        this.setState({isSponsorDrawerVisible: false})
    }
    handleBecomeSponsor = () => {
        sendAnalyze('press_become_sponsor')
        window.location.href="mailto:eggyoyo4859@gmail.com"
    }
    handleProductTypeChange = (value) => {
        this.setState({productFilterType:value},()=>{
            this.anonymousMode()
        })
    }

    renderEditShopModal() {
        return (<EditShopModalForm
            visible={this.state.isEditModalVisible}
            data={this.state.data}
            language={this.state.language}
            onSubmit={this.handleEditModalOk}
            onCancel={this.handleModalCancel}
            imageUploadUrl={this.state.imageUploadUrl}
            onUpload={this.handleUploadChange}
            isUploadWaiting={this.state.isUploadWaiting}
            />)
    }

    renderInfoPopupModal() {
      return (<InfoPopup
            visible={this.state.isInfoModalVisible}
            data={this.state.selectedMarker}
            language={this.state.language}
            onGoogleMap={this.handleGoogleMap}
            onCall={this.handleCall}
            onCancel={this.handleModalCancel}
            user={this.state.currentUser}
          />)
  }

    renderCreateShopModal() {
        return (<CreateShopModalForm
            visible={this.state.isCreateModalVisible}
            language={this.state.language}
            onSubmit={this.handleCreateModalOk}
            onCancel={this.handleModalCancel}
            imageUploadUrl={this.state.imageUploadUrl}
            onUpload={this.handleUploadChange}
            isUploadWaiting={this.state.isUploadWaiting}
            />)
    }
    renderSponsorDrawer() {
        return (<SponsorDrawer
            visible={this.state.isSponsorDrawerVisible}
            data={this.state.sponsorData}
            language={this.state.language}
            onCancel={this.handleSponsorCancel}
            onSponsor={this.handleBecomeSponsor}
            fund={this.state.fund}
            cost={this.state.cost}
            />
            )
    }

    renderClientDrawer() {
        const {language} = this.state
        return (
            <Drawer
                title={`${localization()[language].findShopHeader} : ${this.state.myShopData.length}`}
                placement='left'
                width={280}
                closable={false}
                onClose={this.onDrawerClose}
                visible={this.state.isDrawerVisible}
                footer={(
                <div style={{
                    textAlign: 'center'
                }}>
                    {this.state.isAuth
                        ? (
                            <Button
                                block
                                type="primary"
                                onClick={() => {
                                this.editorMode()
                            }}>โหมดแก้ไขข้อมูล</Button>
                        )
                        : (
                            <div>
                                <p
                                    style={{
                                    fontSize: 13
                                }}>{localization()[language].menuLoginFooterString}</p>
                                <Button block type="primary" onClick={this.facebookAuth}>Log in with Facebook</Button>
                                <Button
                                    block
                                    type="danger"
                                    style={{
                                    marginTop: 12
                                }}
                                    onClick={this.googleAuth}>Log in with Gmail</Button>
                            </div>
                        )}
                </div>
            )}>
                <Select
                    onChange={this.handleProductTypeChange}
                    style={{
                    width: '100%',
                    marginBottom:12,
                }}
                        defaultValue={null}>
                        <Option value={null}>All</Option>
                        <Option value={0}>{localization()[language].normalMask}</Option>
                        <Option value={1}>{localization()[language].n95mask}</Option>
                        <Option value={2}>{localization()[language].cottonMask}</Option>
                        <Option value={3}>{localization()[language].filter}</Option>
                        <Option value={4}>{localization()[language].alcGel}</Option>
                    </Select>
                <Select
                    showSearch
                    style={{
                    width: '100%',
                    marginBottom:30
                }}
                    placeholder={localization()[language].findShopInputPlaceHolder}
                    optionFilterProp="children"
                    onChange={this.onSearchChange}
                    onFocus={this.onSearchFocus}
                    onBlur={this.onSearchBlur}
                    onSearch={this.onSearch}
                    filterOption={(input, option) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
                    {this.state.myShopData.map((data)=>{
                      return(<Option value={data.id}>{data.name}</Option>)
                    })}
                </Select>
                <List
                    size="small"
                    header={localization()[language].lowerTxt}
                    dataSource={this.state.top3PriceData}
                    renderItem={item => <List.Item>
                    <a
                        href="#"
                        onClick={() => {
                        this.setState({
                            selectedMarker: item
                        }, () => {
                          this.showModal('info')
                          this.onDrawerClose();
                          this.flyToId(item.id)
                        })
                    }}>{`${item.name} - ${item.price} ${localization()[language].priceUnit}`}</a>
                </List.Item>}/>
                <List
                    size="small"
                    header={localization()[language].higherTxt}
                    dataSource={this.state.top3StockData}
                    renderItem={item => <List.Item>
                    <a
                        href="#"
                        onClick={() => {
                        this.setState({
                            selectedMarker: item
                        }, () => {
                          this.showModal('info')
                          this.onDrawerClose();
                          this.flyToId(item.id)
                        })
                    }}>{`${item.name} - ${item.quantity} ${localization()[language].quantityUnit}`}</a>
                </List.Item>}/>
            </Drawer>
        )
    }
   
    renderAdminDrawer() {
        const {language} = this.state
        return (
            <Drawer
                title={localization()[language].myShopDataHeader}
                placement='left'
                width={280}
                closable={false}
                onClose={this.onDrawerClose}
                visible={this.state.isDrawerVisible}
                footer={(
                <div style={{
                    textAlign: 'center'
                }}>
                    <Button
                        block
                        type="primary"
                        onClick={() => {
                        this.anonymousMode()
                    }}>{localization()[language].anonymousMode}</Button>
                    <Button
                        block
                        onClick={this.handleLogout}
                        style={{
                        marginTop: 12
                    }}>Log out</Button>
                </div>
            )}>
                <List
                    size="small"
                    dataSource={this.state.myShopData}
                    renderItem={item => <List.Item>
                    <a
                        href="#"
                        onClick={() => {
                          
                          item.lat = item.geo.F
                          item.lng = item.geo.V
                        this.setState({
                            data: item
                        }, () => {
                          console.log(`this.state.data:${JSON.stringify(this.state.data)}`);
                          this.showModal('edit')
                          this.onDrawerClose();
                        })
                    }}>{item.name}</a>
                </List.Item>}/>
            </Drawer>
        )
    }

    getUserLocation = async()=>{
        sendAnalyze('press_find')

      if (navigator.geolocation) { 
        this.setState({isFindBtnLoading:true})
        navigator.geolocation.getCurrentPosition(async (position,error)=> { 
          console.log(position.coords.latitude, position.coords.longitude); 
          if(position.coords.latitude){
            const res = await findShopNearMe(position.coords.latitude,position.coords.longitude)
            if(res.type === 'success'){
              this.setState({isFindBtnLoading:false})
              const shopId = res.results.id
              this.flyToId(shopId)
            }else{
              this.setState({isFindBtnLoading:false})
              message.error(res.msg)
            }
            console.log(`Near me : ${JSON.stringify(res)}`); 
          }else{
            this.setState({isFindBtnLoading:false})
            message.error('กรุณากดปุ่มหาตำแหน่งของคุณ ด้าน ขวาบน')
          }
          
        }); 
      }else{
        message.error('Web Browser นี้ไม่รองรับการค้นหาตำแหน่ง')
      }
    }
    renderFindNearBtn() {
        const {language} = this.state
        return (
            <Button
                className="shadow-darken25"
                style={{
                textAlign: 'center'
            }}
                loading={this.state.isFindBtnLoading}
                shape="round"
                size="large"
                onClick={() => {
                console.log('test');
                this.getUserLocation()
            }}>{localization()[language].findButton}</Button>
        )
    }
    renderCreateShopBtn() {
        const {language} = this.state
        return (
            <Button
                className="shadow-darken25"
                style={{
                textAlign: 'center'
            }}
                shape="round"
                size="large"
                onClick={() => {
                console.log('test');
                this.showModal('create')
            }}>{localization()[language].pinButton}</Button>
        )
    }

    render() {
        const {language,vipSponsorIndex,vipSponsorData} = this.state

        return (
            <div>
                <div ref={this.mapRef} className="absolute top right left bottom"/> {this.state.isMainLoad
                    ? (
                        <div
                            style={{
                            textAlign: 'center',
                            position: 'absolute',
                            top: '50%',
                            left: '50%',
                            transform: 'translate(-50%, -50%)'
                        }}>
                            <Spin size="large"/>
                        </div>
                    )
                    : (
                        <div>
                        {this.renderCreateShopModal()}
                            {this.renderEditShopModal()}
                            {this.renderInfoPopupModal()}
                            {this.renderSponsorDrawer()}
                            <div
                                className="absolute top left ml12 mt12 border border--2 border--white bg-white shadow-darken25 z1 round">
                                <Button
                                    type="link"
                                    style={{
                                    width: 36,
                                    height: 28,
                                    textAlign: 'center'
                                }}
                                    icon={<MenuOutlined />}
                                    onClick={() => {
                                    console.log('test');
                                    this.onShowDrawer()
                                }}></Button>
                            </div>
                            <div
                                className="absolute top right mr60 mt12 border border--2 border--white bg-white shadow-darken25 z1 round">
                                <Radio.Group onChange={this.onLanguageChange} value={this.state.language} size="small" >
                                <Radio.Button value="th">TH</Radio.Button>
                                <Radio.Button value="en">EN</Radio.Button>
                                </Radio.Group>
        
                            </div>
                            <div
                                className="absolute top left ml60 mt12 shadow-darken25 z1 round">
                                <Button
                                    onClick={() => {
                                    sendAnalyze('press_sponsor')
                                    this.setState({isSponsorDrawerVisible:true})
                                }}>{localization()[language].sponsorTxt}</Button>
                            </div>
                            <div
                                className="absolute bottom left ml6 mb36 shadow-darken25 z1 round-full">
                                <Button
                                    icon={<QuestionCircleOutlined />}
                                    shape="circle"
                                    onClick={() => {
                                        legendConfig.duration = 0
                                        notification.open(legendConfig);
                                        sendAnalyze('press_legend')
                                }}></Button>
                            </div>
                            <div className="absolute bottom right left align-center mb18">
                                {this.state.isAuth && !this.state.isAnonymousMode
                                    ? this.renderCreateShopBtn()
                                    : this.renderFindNearBtn()}
                            </div>
                            <div className="absolute bottom right align-center mb24 mr6">
                                {this.state.vipSponsorData.length > 0 
                                    ? (
                                        <div>
                                            <img key={vipSponsorData[vipSponsorIndex].name} src={vipSponsorData[vipSponsorIndex][language==='th'?'picUrl':'picUrl-en'] || vipSponsorData[vipSponsorIndex].picUrl } className="vipSponsor" onClick={()=>{
                                                sendAnalyze('open_big_sponsor')
                                                window.open(vipSponsorData[vipSponsorIndex].url,'_blank')
                                            }}></img>
                                        </div>
                                    )
                                    : null}
                            </div>
                            {this.state.isAuth && !this.state.isAnonymousMode
                                ? this.renderAdminDrawer()
                                : this.renderClientDrawer()}
                        </div>
                    )}
            </div>
        )
    }
}
